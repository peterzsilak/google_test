@search
Feature: Search
  In order to 
  As a user
  I want to be able to find things on the web
  
  Scenario Outline: Search on the web
    Given I am on the google search site
    When I type <something> to the input field
    Then I have to see the result regarding to <something>

    Examples:
    | something |
    | cat       |
    | dog       |
    
    
  
  
  