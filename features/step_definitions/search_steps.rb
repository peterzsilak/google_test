#Encoding utf-8

Given(/^I am on the google search site$/) do
  puts 'I am on the google search page'
  visit('http://www.google.com')
end

When(/^I type (.*) to the input field$/) do | something |
  puts "Searching for #{something}"
  search(something)
end

Then(/^I have to see the result regarding to (.*)$/) do | something |
  puts "Results displays regarding to the #{something}"
  page.assert_selector('div', :text => something, :visible => true)
end