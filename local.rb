ENV['TEST_ENV'] ||= 'local'
ENV['BROWSER'] ||= 'mobile'
ENV['ORIENTATION'] ||= 'PORTRAIT'
ENV['DATA'] ||= ENV['TEST_ENV']
ENV['BROWSERMOB_PROXY'] ||= 'false'

### Settings which cannot be altered with environment variables ###
BROWSERMOB_PROXY_PORT = 8181
BROWSERMOB_PROXY_LOGGING = true